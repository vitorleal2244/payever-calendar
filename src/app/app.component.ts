import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

const logStr = '[AppComponent]';

/**
 * App component
 *
 * @export
 * @class AppComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  dates: Array<Date> = [];

  /**
   * Creates an instance of AppComponent
   *
   * @param {DataService} date
   * @memberof AppComponent
   */
  constructor(private readonly date: DataService) {}

  /**
   * Initializes app
   *
   * @memberof AppComponent
   */
  ngOnInit() {
    this.initCalendarDates();
  }

  /**
   * Initiates the calendar
   *
   * @returns
   * @memberof AppComponent
   */
  private initCalendarDates() {
    try {
      const { year, monthIndex } = this.date.getCurrent();

      this.dates = [
        new Date(year, monthIndex)
      ];
    } catch (cause) {
      console.error(`${logStr} initCalendarDates() ««« ${JSON.stringify(cause)}`);
    }
  }
}
