/*
 * File: data.service.ts
 * Project: calendar-payever
 * File Created: Monday, 27th March 2023 8:06:32 am
 * Author: vitorleal2244
 * -----
 * Last Modified: Monday, 27th March 2023 11:43:44 am
 * Modified By: vitorleal2244
 * -----
 * Copyright 2023 Vitor Leal
 */

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

const logStr = "[DataService]";

/**
 * DataService
 *
 * @export
 * @class DataService
 */
@Injectable()
export class DataService {
  public testData: Array<any>;

  /**
   * Creates an instance of DataService
   *
   * @memberof DataService
   */
  constructor() {
    const { year, monthIndex } = this.getCurrent();
    let year1 = year;
    let monthIndex1 = monthIndex - 1;

    let year2 = year;
    let monthIndex2 = monthIndex + 1;

    if (monthIndex === 0) {
      year1 = year - 1;
      monthIndex1 = 11;
    }

    if (monthIndex === 11) {
      year2 = year + 1;
      monthIndex2 = 0;
    }

    this.testData = [];
  }

  /**
   * Get the current year and month
   *
   * @returns
   * @memberof DataService
   */
  public getCurrent(): { year: number; monthIndex: number } {
    const currentDate = new Date();

    return {
      year: currentDate.getFullYear(),
      monthIndex: currentDate.getMonth()
    };
  }

  /**
   * Get the events for a specific year and month
   *
   * @param {number} year
   * @param {number} month
   * @returns
   * @memberof DataService
   */
  public getSchedulesFor(year: number, month: number): Observable<any[]> {
    if (this.testData) {
      return of(this.testData)
        .pipe(
          map(arr => {
            return arr.filter(
              event => event.date.getFullYear() === year && event.date.getMonth() === month
            );
          })
        );
    }

    return of();
  }

  /**
   * Add new event to data service
   *
   * @param {string} eventTitle
   * @param {Date} eventDate
   * @returns
   * @memberof DataService
   */
  public addNewSchedule(eventTitle: string, eventDate: Date): boolean {
    if (eventTitle && eventDate) {
      var dateDay = eventDate.getDay();
      var dateMonth = eventDate.getMonth();
      var dateYear = eventDate.getFullYear();

      try {
        var answer = {title: eventTitle, date: eventDate};

        this.testData = [...this.testData, answer];
        console.log(this.testData);
        return true;
      } catch (cause) {
        console.log(`${logStr} addNewSchedule() ««« ${JSON.stringify(cause)}`)
      }
    }

    return false;
  }
}
