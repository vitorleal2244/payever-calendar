/*
 * File: add-calendar-dialog.component.ts
 * Project: calendar-payever
 * File Created: Monday, 27th March 2023 10:41:19 am
 * Author: vitorleal2244
 * -----
 * Last Modified: Monday, 27th March 2023 11:14:26 am
 * Modified By: vitorleal2244
 * -----
 * Copyright 2023 Vitor Leal
 */

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { DataService } from '../../../app/data.service';

const logStr = '[AddCalendarDialogComponent]';

/**
 * AddCalendarDialogComponent
 *
 * @export
 * @class AddCalendarDialogComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-add-calendar-dialog',
  templateUrl: './add-calendar-dialog.component.html',
  styleUrls: ['./add-calendar-dialog.component.css']
})
export class AddCalendarDialogComponent implements OnInit {
    public formData: any;
    public eventTitle: string;
    public eventDate: Date;
    public dialogMessage: any;

    /**
     * Creates an instance of AddCalendarDialogComponent
     *
     * @param {DataService} dataService
     * @memberof AddCalendarDialogComponent
     */
    constructor(private dataService: DataService) {
      this.formData = {};
      this.eventTitle = '';
      this.eventDate = new Date('27-03-2023');
      this.dialogMessage = {
        message: '',
        status: 'error'
      };
    }


    /**
     * Sets the formGroup
     *
     * @memberof AddCalendarDialogComponent
     */
    ngOnInit(): void {
      this.formData = new FormGroup({
        eventTitle: new FormControl(""),
        eventDate: new FormControl("")
      });
    }

    /**
     * Triggered when the user submits the add event form
     *
     * @param {any} data
     * @returns
     * @memberof CalendarComponent
     */
    public onSubmitForm(data: any): void {
      try {
        this.eventTitle = data.eventTitle;
        this.eventDate = data.eventDate;

        let wasEventAdded = this.dataService.addNewSchedule(this.eventTitle, this.eventDate);

        if (wasEventAdded) {
          this.dialogMessage = {
            message: 'Event added with success!',
            status: 'success'
          };
        } else {
          this.dialogMessage = {
            message: 'Error when trying to add event!',
          };
        }

        this.cleanAlertBox();
      } catch (cause) {
        console.error(`${logStr} onSubmitForm() ««« ${JSON.stringify(cause)}`)
      }
    }


    /**
     * Removes alert box after 2 seconds being shown
     *
     * @returns
     * @memberof AddCalendarDialogComponent
     */
    public cleanAlertBox(): void {
      setTimeout(() => {
        this.dialogMessage.message = '';
      }, 2000);
    }
}
