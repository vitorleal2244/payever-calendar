/*
 * File: calendar.component.ts
 * Project: calendar-payever
 * File Created: Monday, 27th March 2023 8:05:10 am
 * Author: vitorleal2244
 * -----
 * Last Modified: Monday, 27th March 2023 10:21:12 am
 * Modified By: vitorleal2244
 * -----
 * Copyright 2023 Vitor Leal
 */

import { Component, Input, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { DataService } from '../../../app/data.service';
import { AddCalendarDialogComponent } from '../../dialogs/add-calendar-dialog/add-calendar-dialog.component';

const logStr = '[CalenderComponent]';

/**
 * Calendar Component
 *
 * @export
 * @class CalendarComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  public localDate: Date;

  public readonly weekDayNames: Array<string> = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];

  public weeksCount: number;
  public weeks: any[] = [];

  public year: number;
  public month: number;
  public dayOfTheWeek: number;

  /**
   * Set the date
   *
   * @Input
   */
  @Input()
  set date(date: Date) {
    this.localDate = date;

    this.year = this.localDate.getFullYear();
    this.month = this.localDate.getMonth();
    const day = 1;

    this.dayOfTheWeek = new Date(this.year, this.month, day).getDay();
    this.weeksCount = this.getWeeksCount(this.localDate);

    for (let i = 0; i < this.weeksCount; ++i) {
      this.weeks.push([{}, {}, {}, {}, {}, {}, {}]);
    }
  }

  /**
   * Creates an instance of CalendarComponent
   *
   * @param {MatDialog} dialog
   * @param {DataService} dataService
   * @memberof CalendarComponent
   */
  constructor(
    public dialog: MatDialog,
    private dataService: DataService
  ) {
    this.localDate = new Date("2023-03-27");
    this.weeksCount = 0;
    this.year = new Date().getFullYear();
    this.month = new Date().getMonth();
    this.dayOfTheWeek = 0;
  }

  /**
   * Gets the events
   *
   * @memberof CalendarComponent
   */
  ngOnInit(): void {
    try {
      this.dataService.getSchedulesFor(this.year, this.month).subscribe(
        schedules => {
          this.fillDaysWithSchedules(this.dayOfTheWeek, schedules);
        }
      );
    } catch (cause) {
      console.error(`${logStr} set date ««« ${JSON.stringify(cause)}`)
    }
  }

  /**
   * Get events from data service
   *
   * @param {any[]} schedules
   * @param {Date} currentDate
   * @returns
   * @memberof CalendarComponent
   */
  private getSchedulesForDay(schedules: any[], currentDate: Date): any[] {
    return schedules
      .filter(x => this.isSameDate(x.date, currentDate))
      .map(x => {
        const tmp = { ...x };
        delete tmp.date;
        return tmp;
      });
  }

  /**
   * Add schedules to a day
   *
   * @param {number} firstDayOfTheMonth
   * @param {any[]} schedules
   * @returns
   * @memberof CalendarComponent
   */
  private fillDaysWithSchedules(firstDayOfTheMonth: number, schedules: any[]) {
    const year = this.localDate.getFullYear();
    const month = this.localDate.getMonth();
    let day = 1;

    for (let week = 0; week < this.weeksCount; ++week) {
      let weekDay = week === 0 ? firstDayOfTheMonth : 0;
      for (; weekDay < this.weekDayNames.length; ++weekDay) {
        const currentDate = new Date(Date.UTC(year, month, day));
        this.weeks[week][weekDay] = {
          date: currentDate,
          day,
          weekDay: this.weekDayNames[weekDay],
          schedules: this.getSchedulesForDay(schedules, currentDate)
        };

        ++day;
        if (new Date(year, month, day).getMonth() !== month) {
          return;
        }
      }
    }
  }

  /**
   * Check if to given dates are exact the same
   *
   * @param {Date} date1
   * @param {Date} date2
   * @returns
   * @memberof CalendarComponent
   */
  private isSameDate(date1: Date, date2: Date) {
    return (
      date1.getFullYear() === date2.getFullYear() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getDate() === date2.getDate()
    );
  }

  /**
   * Get the number of weeks of a given month
   *
   * @param {Date} date
   * @returns
   * @memberof CalendarComponent
   */
  private getWeeksCount(date: Date): number {
    const first = new Date(date.getFullYear(), date.getMonth(), 1);
    const last = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    const daysCount = first.getDay() + last.getDate();
    const weeksCount = Math.ceil(daysCount / 7);

    return weeksCount;
  }


  /**
   * Opens a dialog to insert a new event
   *
   * @memberof CalendarComponent
   * @returns
   */
  public showAddMorePopup(): void {
    this.dialog.open(AddCalendarDialogComponent, {
      height: '400px',
      width: '600px',
    });
  }
}
