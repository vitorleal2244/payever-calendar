/*
 * File: header.component.ts
 * Project: calendar-payever
 * File Created: Monday, 27th March 2023 2:17:38 pm
 * Author: vitorleal2244
 * -----
 * Last Modified: Monday, 27th March 2023 2:22:30 pm
 * Modified By: vitorleal2244
 * -----
 * Copyright 2023 Vitor Leal
 */

import { Component } from '@angular/core';

/**
 * HeaderComponent
 *
 * @export
 * @class HeaderComponent
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {}
