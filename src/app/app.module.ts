/*
 * File: app.module.ts
 * Project: calendar-payever
 * File Created: Monday, 27th March 2023 2:17:38 pm
 * Author: vitorleal2244
 * -----
 * Last Modified: Monday, 27th March 2023 2:31:29 pm
 * Modified By: vitorleal2244
 * -----
 * Copyright 2023 Vitor Leal
 */
import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog'
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';

import { HeaderComponent } from './components/header/header.component';
import { CalendarComponent } from './components/calendar/calendar.component';

import { AddCalendarDialogComponent } from './dialogs/add-calendar-dialog/add-calendar-dialog.component';

import { DataService } from './data.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CalendarComponent,
    AddCalendarDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NoopAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatDatepickerModule,
    MatIconModule,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
