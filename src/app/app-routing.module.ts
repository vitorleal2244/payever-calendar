/*
 * File: app-routing.module.ts
 * Project: calendar-payever
 * File Created: Monday, 27th March 2023 2:17:38 pm
 * Author: vitorleal2244
 * -----
 * Last Modified: Monday, 27th March 2023 2:37:21 pm
 * Modified By: vitorleal2244
 * -----
 * Copyright 2023 Vitor Leal
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './components/calendar/calendar.component';

const routes: Routes = [
  {
    path: '', component: CalendarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
